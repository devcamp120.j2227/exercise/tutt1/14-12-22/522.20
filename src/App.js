import "./App.css";
import Caculator from "./components/styledButtonCaculator";
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';

function App() {
  return (
    <div className="div-main">
      <div className="div-card">
        <div className="div-header">
          <h3 className="phep-tinh">2536 + 419 + </h3>
        </div>
        <div className="div-body">
          <h1 className="ket-qua">2955</h1>
        </div>
        <div className="div-footer">
          <Caculator style={{color: "rgb(214,92,80)"}}>C</Caculator>
          <Caculator>≠</Caculator>
          <Caculator>%</Caculator>
          <Caculator>/</Caculator>
          <Caculator type="normal">7</Caculator>
          <Caculator type="normal">8</Caculator>
          <Caculator type="normal">9</Caculator>
          <Caculator>x</Caculator>
          <Caculator type="normal">4</Caculator>
          <Caculator type="normal">5</Caculator>
          <Caculator type="normal">6</Caculator>
          <Caculator>-</Caculator>
          <Caculator type="normal">1</Caculator>
          <Caculator type="normal">2</Caculator>
          <Caculator type="normal">3</Caculator>
          <Caculator>+</Caculator>
          <Caculator type="normal">.</Caculator>
          <Caculator type="normal">0</Caculator>
          <Caculator type="normal"><ArrowBackIosNewIcon/></Caculator>
          <Caculator>=</Caculator>
        </div>
      </div>
    </div>
  );
}

export default App;