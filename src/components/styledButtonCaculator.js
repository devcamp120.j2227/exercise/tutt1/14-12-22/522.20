import styled from "styled-components";

const Caculator = styled.button`
    width: 77px;
    height: 77px;
    font-size: 18px;
    border: 0.5px solid rgb( 58, 70, 85 );
    color: ${props => props.color};
    ${props => props.type && props.type === "normal" ?
        `background-color: rgb(66,80,98); color: white;` :
        `background-color: rgb(64,77,94); color:rgb(173,175,180);`
    }
`

export default Caculator;